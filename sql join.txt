SOAL 1

a. SELECT id, name, email FROM users
   ;

b. -SELECT * FROM `items` WHERE price > 1000000
    ;
   -SELECT * FROM `items` WHERE name LIKE '%sang%'
    ;

c. SELECT items.name, description, price, stock, category_id, categories.name AS kategori
   from items
   inner JOIN categories
   ON items.category_id = categories.id
   ;

SOAL 2

1. UPDATE items
   SET price = '2500000'
   WHERE items.id = 1;
